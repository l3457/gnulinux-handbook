# Gentoo Handbook

Gentoo, Linux temelli linux dağıtımlarından biridir herhangi bir dağıtımdan çatallanmamıştır.

Kurulum Manual yapılmaktadır. Herhangi bir kurulum aracı yoktur. Kurulum için çok ayrıntılı bir dökümana sahip dökümandaki adımları uygulayarak kurulumu gerçekleştiririz. [Installing Handbook amd64](https://wiki.gentoo.org/wiki/Handbook:Parts/Full/Installation)

Gentoo en çok mimariyi destekleyen linux dağıtımıdır. Ve her birinin dökümanları sitesinde mevcuttur. 

Desteklediği mimariler;

- amd64
- x86
- alpha
- arm
- arm64
- hppa
- ia64
- ppc
- riscv
- sparc
- s390

Şuan gentoo şu init sistemlerinde iso şeklinde indirilebiliyor;

- OpenRC
- Systemd

## Kurulum

### Aşamaları

1. Klavyeyi ayarla(/etc/conf.d/keymaps)

1. İnterneti bağla(net-setup)

1. Tarih'i ve zaman'ı doğru ayarla(date)

1. Diski bölümle(cfdisk) ve formatla(mkfs)

1. Diski bağla

1. stage paketi indir diske çıkart veya live rootfs kopyala

1. chroot ile gir.

1. portage`i config et, repo'ları güncelle

1. eselect profile seç

1. sistemi seçilen profile göre paket indirmesi olan paketleri güncellemesi için portage paket yöneticisi kullan

1. kernel config ederken sistemin özelliklerine bakman için gerekli olan lspci komutunu kullanmak için bu paketi kur(sys-apps/pciutils)

1. kernel config et, derle ve kur(===> /usr/src/linux-version)

1. initramfs oluşturabilmek için genkernel tool'unu kur

1. initramfs oluştur.

1. firmware kur

1. ağ için kullanacağın paketleri kur, config et.

1. grub paketini kur

1. grub`u diske kur

